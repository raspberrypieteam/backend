/*
 * LazyLoader.cpp
 *
 *  Created on: 13. 2. 2017
 *      Author: payne
 */

#include "LazyLoader.h"

LazyLoader::LazyLoader() :
	_module(NULL),
	_init(NULL)
{}

LazyLoader::LazyLoader(std::string &path) :
	modulePath(path),
	_module(NULL),
	_init(NULL)
{}

LazyLoader::LazyLoader(const LazyLoader &copyFrom) :
    modulePath(copyFrom.modulePath),
	_module(NULL),
	_init(NULL)
{}

void LazyLoader::operator=(const LazyLoader &copyFrom) {
	modulePath = copyFrom.modulePath;
}

void *LazyLoader::module() {
	if(!modulePath.size()) {
		std::cerr << "Error:\t'" << modulePath << "'" << std::endl;

		return NULL;
	}
	if(!_module) {
			_module = dlopen(modulePath.c_str(), RTLD_NOW);
			if(!_module) {
				std::cerr << "Error:\tUnable to load module '" << modulePath << "': " << dlerror() << std::endl;
				exit(EXIT_FAILURE);
			}
	}
	dlerror();
	return _module;

}

void LazyLoader::init() {
	*(void **) (&_init) = dlsym(module(), "initModule");
	char *error;
	if ((error = dlerror()) != NULL)  {
		std::cerr << error << std::endl;
	    exit(EXIT_FAILURE);
	}
	Module *m = dynamic_cast<Module *>((*_init)());
}

LazyLoader::~LazyLoader() {
	if(_module) {
		dlclose(_module);
		_module = NULL;
	}
}

