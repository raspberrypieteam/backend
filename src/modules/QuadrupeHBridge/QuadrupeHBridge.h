/*
 * module.h
 *
 *  Created on: 14. 2. 2017
 *      Author: payne
 */

#ifndef SRC_MODULES_QUADRUPEHBRIDGE_QUADRUPEHBRIDGE_H_
#define SRC_MODULES_QUADRUPEHBRIDGE_QUADRUPEHBRIDGE_H_

#include <RaspberryPi-common/Module.h>

#include <iostream>



extern "C" Module *initModule();

class QuadrupeHBridge : public Module {
public:
	QuadrupeHBridge();
	virtual ~QuadrupeHBridge();

};



#endif /* SRC_MODULES_QUADRUPEHBRIDGE_QUADRUPEHBRIDGE_H_ */
