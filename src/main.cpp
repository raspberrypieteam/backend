

#include "RaspberryPiConfig.h"
#include "LazyLoader.h"

#include <RaspberryPi-common/common.h>

#include <iostream>
#include <map>
#include <string>
#include <regex>


#include <dirent.h>

void findModules();

std::map<std::string, LazyLoader> availableModules;

int main(int argc, char **argv) {
	findModules();
	return 0;
}

void findModules() {
	std::string modulesDir(getBasePath());
	modulesDir.append("modules");

	size_t len = modulesDir.size();
	DIR *dirD = opendir(modulesDir.c_str());
	struct dirent *dp = NULL;

	std::regex libNameRegex("lib(.)*\\.so");
	while ((dp = readdir(dirD)) != NULL) {
		if(std::regex_match(dp->d_name, libNameRegex)) {
			std::string moduleName(dp->d_name);

			std::string modulePath(modulesDir);
			modulePath.append("/");
			modulePath.append(dp->d_name);

			LazyLoader loader(modulePath);
			availableModules.insert(std::make_pair(moduleName.substr(3, moduleName.size() - 6), loader));
		}
	}
	closedir(dirD);

	availableModules["QuadrupeHBridge"].init(); //TODO delete - test

	return;
}

