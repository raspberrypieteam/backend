/*
 * Module.h
 *
 *  Created on: 13. 2. 2017
 *      Author: payne
 */

#ifndef SRC_RASPBERRYPI_MODULE_H_
#define SRC_RASPBERRYPI_MODULE_H_

class Module {
public:
	Module();
	virtual ~Module();
};

#endif /* SRC_RASPBERRYPI_MODULE_H_ */
