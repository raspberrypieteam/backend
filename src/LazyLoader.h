/*
 * LazyLoader.h
 *
 *  Created on: 13. 2. 2017
 *      Author: payne
 */

#ifndef SRC_LAZYLOADER_H_
#define SRC_LAZYLOADER_H_

#include <RaspberryPi-common/Module.h>

#include <string>
#include <iostream>
#include <cstdlib>

#include <dlfcn.h>

class LazyLoader {
private:
	std::string modulePath;
	void *_module;

	Module *(*_init)();
public:
	LazyLoader();
	explicit LazyLoader(std::string &);
	LazyLoader(const LazyLoader &);

	void operator=(const LazyLoader &);

	void *module();
	void init();
	virtual ~LazyLoader();
};

#endif /* SRC_LAZYLOADER_H_ */
