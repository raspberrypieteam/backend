#include "common.h"


std::string getBasePath() {
	char result[PATH_MAX];
	size_t count = readlink( "/proc/self/exe", result, PATH_MAX);
	std::string tmp( result, (count > 0) ? count : 0 );
	size_t found = tmp.find_last_of("/");
	return tmp.size() ? tmp.substr(0, found + 1) : tmp;
}
